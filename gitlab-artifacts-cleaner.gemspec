
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "gitlab/artifacts/cleaner/version"

Gem::Specification.new do |spec|
  spec.name          = "gitlab-artifacts-cleaner"
  spec.version       = Gitlab::Artifacts::Cleaner::VERSION
  spec.authors       = ["Shinya Maeda"]
  spec.email         = ["shinya@gitlab.com"]

  spec.summary       = 'gitlab job artifacts cleaner'
  spec.description   = 'gitlab job artifacts cleaner'
  spec.homepage      = 'https://gitlab.com/dosuken123/gitlab-artifacts-cleaner'
  spec.license       = "MIT"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "bin"
  spec.executables   = 'gitlab-artifacts-cleaner'
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.17"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "httparty", "~> 0.17.1"
end
