require "gitlab/artifacts/cleaner/version"
require 'httparty'

module Gitlab
  module Artifacts
    module Cleaner
      extend self

      def execute
        @api_route, @priavte_token, @older_than = ARGV
        @current_page = 1
        @older_than = Time.parse(@older_than)
        @total_count = 0

        raise 'Insufficient arguments' unless @api_route && @priavte_token && @older_than

        while jobs = get_job_list!
          puts "@current_page: #{@current_page}"

          old_jobs = jobs.select { |job| Time.parse(job['created_at']) < @older_than }

          old_jobs.each do |job|
            job_id = job['id']
            puts "job_id: #{job_id} message: deleting job artifacts"
            delete_job_artifact!(job_id)
            puts "job_id: #{job_id} message: deleted job artifacts"
            @total_count += 1
          end
        end

        puts "DONE. @total_count: #{@total_count}"
      end

      def done?
        return false unless @total_page

        @current_page > @total_page
      end

      def get_job_list!
        response = ::HTTParty.get(job_index_api, headers: headers, query: query)

        raise "Failed to get job list: #{response}" unless response.code == 200

        return nil if response.empty?

        response
      ensure
        @current_page += 1
      end

      def delete_job_artifact!(job_id)
        ::HTTParty.delete(delete_artifact_api(job_id), headers: headers).tap do |response|
          raise "Failed to delete job artifact: #{response}" unless response.code == 204
        end
      end

      def headers
        { 'Private-token' => @priavte_token }
      end

      def query
        { page: @current_page, per_page: 100 }
      end

      def job_index_api
        "#{@api_route}/jobs"
      end

      def delete_artifact_api(job_id)
        "#{@api_route}/jobs/#{job_id}/artifacts"
      end
    end
  end
end
